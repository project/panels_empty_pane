<?php

/**
 * @file
 * A partner to the Empty Pane access rule.
 */

$plugin = array(
  'title' => t('Pane-is-empty companion'),
  'cache get' => 'pane_empty_cache_get_cache',
  'cache set' => 'pane_empty_cache_set_cache',
  'cache clear' => 'pane_empty_cache_clear_cache',
);

function pane_empty_cache_get_cache($conf, $display, $args, $contexts, $pane = NULL) {
  // dpm(__FUNCTION__);
  $panes = &drupal_static('panels_empty_pane', array());

  $first_time = &drupal_static('panels_empty_pane_first_time', TRUE);

  // First time through, generate the panes for this display.
  if ($first_time && empty($panes)) {
    $panes = panels_empty_pane_initialize_cache($display, $pane);
    $first_time = FALSE;
  }

  // dpm($panes);
  // dpm($pane->uuid);
  if (!empty($panes[$pane->uuid])) {
    $cache = new panels_cache_object();
    $cache->set_content($panes[$pane->uuid]);
    $cache->ready = TRUE;
    $cache->head = array();
    $cache->css = array();
    $cache->js = array();
    // dpm($cache);
    return $cache;
  }
}

function pane_empty_cache_set_cache($conf, $content, $display, $args, $contexts, $pane = NULL) {
  // dpm(__FUNCTION__);
  $panes = &drupal_static('panels_empty_pane', array());

  $panes[$pane->uuid] = $content->content;
}

function pane_empty_cache_clear_cache($display) {
  // Clear the static cache.
  drupal_static_reset('panels_empty_pane', array());

  // Reinitialize the cache.
  $panes = &drupal_static('panels_empty_pane', array());
}
