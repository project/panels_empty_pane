<?php

/**
 * @file
 * Plugin to provide access control based upon role membership.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Pane is empty'),
  'description' => t('Control access by .'),
  'callback' => 'panels_empty_pane_ctools_access_check',
  'default' => array('panes' => array()),
  'settings form' => 'panels_empty_pane_ctools_access_settings',
  'settings form submit' => 'panels_empty_pane_ctools_access_settings_submit',
  'summary' => 'panels_empty_pane_ctools_access_summary',
  'category' => '',
);

/**
 * Settings form for the 'by role' access plugin
 */
function panels_empty_pane_ctools_access_settings($form, &$form_state, $conf) {
  $this_pane = $form_state['pane'];

  // Build a list of all of the other panes on this page.
  $panes = array();
  foreach ($form_state['display']->content as $name => $pane) {
    // Don't include the pane being edited in the list, because recursion sucks.
    if ($this_pane->pid == $name) {
      continue;
    }
    $content_type = ctools_get_content_type($pane->type);
    $panes[$pane->uuid] = ctools_content_admin_title($content_type, $pane->subtype, $pane->configuration, $form_state['display']->context);
    if (empty($panes[$pane->uuid])) {
      $panes[$pane->uuid] = t('@type: @subtype (missing)', array('@type' => $pane->type, '@subtype' => $pane->subtype));
    }
  }

  $form['settings']['panes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('If these panes are empty, this pane will be visible'),
    '#default_value' => !empty($conf['panes']) ? $conf['panes'] : array(),
    '#options' => $panes,
    '#required' => TRUE,
    '#description' => t('It is recommended to use the "Pane-is-empty companion" cache plugin to avoid these panes being generated multiple times.'),
  );

  return $form;
}

/**
 * Compress the roles allowed to the minimum.
 */
function panels_empty_pane_ctools_access_settings_submit($form, &$form_state) {
  $form_state['values']['settings']['panes'] = array_keys(array_filter($form_state['values']['settings']['panes']));
}

/**
 * Check for access.
 */
function panels_empty_pane_ctools_access_check($conf, $context, $plugin) {
  // dpm(__FUNCTION__);
  // If no panes were defined then access is approved.
  if (empty($conf['panes'])) {
    return TRUE;
  }
  // dpm(func_get_args());
  // ddebug_backtrace();

  $panes = &drupal_static('panels_empty_pane', array());

  // Render the display's panes.
  if (empty($panes)) {
    $panes = panels_empty_pane_initialize_cache();
  }

  // Identify whether the pane should be rendered.
  $access = TRUE;
  // dpm($panes);
  foreach ($conf['panes'] as $pane_id) {
    if (!empty($panes[$pane_id])) {
      $access = FALSE;
    }
  }
  // dpm("Access is: " . ($access ? 'TRUE' : 'FALSE'));
  // drupal_set_message('<hr />');

  return TRUE;//$access;
}

/**
 * Provide a summary description based upon the checked roles.
 */
function panels_empty_pane_ctools_access_summary($conf, $context) {
  if (!empty($conf['panes'])) {
    if (count($conf['panes']) > 1) {
      return t('Panes are empty: @panes', array('@panes' => implode(',', $conf['panes'])));
    }
    else {
      return t('Pane is empty: @panes', array('@panes' => implode(',', $conf['panes'])));
    }
  }
  else {
    return t('Error.');
  }
}
