Panels Empty Pane
-----------------
This module provides an access rule for Panels that will cause a pane to be
output only if one of the selected panes is empty.

To avoid causing panes being rendered multipe times, an included cache plugin
may be used for the panes that are being checked.


Credits / contact
--------------------------------------------------------------------------------
Developed and maintained by Damien McKenna [1].

Ongoing development is sponsored by Mediacurrent [2].

The best way to contact the author is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
  https://www.drupal.org/project/issues/panels_empty_pane


References
--------------------------------------------------------------------------------
1: https://www.drupal.org/u/damienmckenna
2: http://www.mediacurrent.com/
